# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import os


def concatenate_files(path, path2):
    name1 = path.split('/')[-1].split('.')[0]
    name2 = path2.split('/')[-1].split('.')[0]
    with open(path, 'r') as f1:
        with open(path2, 'r') as f2:
            with open('GGL/cvs.fasta') as cvs_file:
                csv = cvs_file.read()
                final = '{}{}{}'.format(csv, f1.read(), f2.read())
                with open('GGL/Combined/{}-{}-Combined.fasta'.format(name1, name2), 'w') as f_final:
                    f_final.write(final)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    for root, dirs, files in os.walk("GGL", topdown=True):
        g_files = [f for f in files if f.find('L') < 0]
        gl_files = []
        for g in g_files:
            x = list(g)
            x.insert(1, 'L')
            f1 = os.path.join(root, g)
            f2 = os.path.join(root, ''.join(x))
            if os.path.exists(f2):
                concatenate_files(f1, f2)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
